# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
# Create your views here.
from services import service
import requests
import json

def orchestrator(request):
	a = request.GET.get('a',0)
	b = request.GET.get('b',0)
	c = request.GET.get('c',0)
	d = request.GET.get('d',0)

	url1 = 'http://host22023.proxy.infralabs.cs.ui.ac.id/cots2/r1?a='+str(a)+'&b='+str(b)+'&c='+str(c)+'&d='+str(d)
	r1 = requests.get(url1)
	data1 = r1.json()
	service1 = data1.get('hasil',0)
	url2 = 'http://host23023.proxy.infralabs.cs.ui.ac.id/cots2/r2?a='+str(a)+'&b='+str(b)+'&c='+str(c)+'&d='+str(d)
	r2 = requests.get(url2)
	data2 = r2.json()
	service2 = data2.get('hasil',0)
	url3 = 'http://host24023.proxy.infralabs.cs.ui.ac.id/cots2/r3?a='+str(a)+'&b='+str(b)+'&c='+str(c)+'&d='+str(d)
	r3 = requests.get(url3)
	data3 = r3.json()
	service3 = data3.get('hasil',0)

	result = float(service1) + float(service2) - float(service3)
	response = { 'hasil' : result}	

	return JsonResponse(response, safe=False)

def webservice1(request):
	# {Round_Number[ (A+B) / C ] } modulo D
	# = [( (A+B) * C) / D ]
	a = request.GET.get('a',0)
	b = request.GET.get('b',0)
	c = request.GET.get('c',0)
	d = request.GET.get('d',0)

	# aPlusb = service.tambah(a,b)
	# divC = service.bagi(aPlusb.get('hasil'),c)
	# rounded = service.roundNumber(divC.get('hasil'))		# COTS 2
	# modulo = service.modulo(rounded,d)
	# response = {'hasil': modulo}

	aPlusb = service.tambah(a,b)
	kaliC = service.kali(aPlusb.get('hasil'),c)				# Tugas 2
	bagiD = service.bagi(kaliC,d)
	response = {'hasil': bagiD.get('hasil')}

	return JsonResponse(response, safe=False)

def webservice2(request):
	# [ (A+D) / (B modulo C)  ] COTS 2
	#  [( (A+B+C) * D) / C ] Tugas 2
	a = request.GET.get('a',0)
	b = request.GET.get('b',0)
	c = request.GET.get('c',0)
	d = request.GET.get('d',0)

	# aPlusd = service.tambah(a,d)
	# modulo = service.modulo(b,c)
	# divMod = service.bagi(aPlusd.get('hasil'),modulo)		# COTS 2
	# response = {'hasil': divMod.get('hasil')}

	aPlusb = service.tambah(a,b)
	plusC = service.tambah(aPlusb.get('hasil'),c)		# Tugas 2
	kaliD = service.kali(plusC.get('hasil'),d)
	bagiC = service.bagi(kaliD,c)
	response = {'hasil': bagiC.get('hasil')}

	return JsonResponse(response, safe=False)

def webservice3(request):
	# Round_Number [ (A-C) * (B / C)  ]
	#  [( (C * B) * D) / A ]
	a = request.GET.get('a',0)
	b = request.GET.get('b',0)
	c = request.GET.get('c',0)
	d = request.GET.get('d',0)

	# aMinc = service.kurang(a,c)
	# bDivc = service.bagi(b,c)
	# kali = service.kali(aMinc,bDivc.get('hasil'))			# COTS 2
	# rounded = service.roundNumber(kali)
	# response = {'hasil': rounded}

	cKalib = service.kali(c,b)
	kaliD = service.kali(cKalib,d)
	bagiA = service.bagi(kaliD,a)
	response = {'hasil': bagiA.get('hasil')}				# Tugas 2

	return JsonResponse(response, safe=False)

# def webservice2(request):
# 	response = service.consumeSOAP(request)

# 	return JsonResponse(response, safe=False)

# def webservice3(request):
# 	response = service.consumeSOAP(request)

# 	return JsonResponse(response, safe=False)

# def webservice4(request):
# 	response = service.consumeSOAP(request)

# 	return JsonResponse(response, safe=False)

# @csrf_exempt
# def login(request):
# 	if request.method == 'GET':
# 		response = service.consumeSOAP(request)
# 	else:
# 		response = {status:"fail", message:"there\'s something error in this app"}

# 	return JsonResponse(response, safe=False)