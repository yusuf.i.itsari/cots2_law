import requests
import json
import zeep

def tambah(x,y):
	url = 'http://host20099.proxy.infralabs.cs.ui.ac.id/tambah.php?a='+str(x)+'&b='+str(y)
	r = requests.get(url)

	data = r.json()

	return data

def kurang(x,y):
	url = 'http://host20099.proxy.infralabs.cs.ui.ac.id/kurang.php'
	payload = {
		'a': x,
		'b': y
	}
	data = requests.post(url, data=payload)

	return data.json()

def kali(x,y):
	url = 'http://host20099.proxy.infralabs.cs.ui.ac.id/kali.php'
	headers = {'Argumen-A': str(x), 'Argumen-B': str(y)} 
	r = requests.get(url, headers=headers)

	data = r.json()

	return data

def bagi(x,y):
	url = 'http://host20099.proxy.infralabs.cs.ui.ac.id/bagi.php'
	headers = {'Argumen-A': str(x), 'Argumen-B': str(y)} 
	r = requests.head(url, headers=headers)

	data = r.headers

	return data

def roundNumber(x):
	wsdl = 'http://host20099.proxy.infralabs.cs.ui.ac.id/matematika.xml'
	# wsdl = 'http://www.soapclient.com/xml/soapresponder.wsdl'
	client = zeep.Client(wsdl=wsdl)
	data = client.service.round_number(float(x))

	return data

def modulo(x,y):
	wsdl = 'http://host20099.proxy.infralabs.cs.ui.ac.id/matematika.xml'
	# wsdl = 'http://www.soapclient.com/xml/soapresponder.wsdl'
	client = zeep.Client(wsdl=wsdl)
	data = client.service.modulo(float(x), float(y))

	return data