from django.conf.urls import url
from .views import orchestrator, webservice1, webservice2, webservice3
#url for app
urlpatterns = [
	url(r'^calculate', orchestrator, name='orchestrator'),
    url(r'^r1', webservice1, name='webservice1'),
    url(r'^r2', webservice2, name='webservice2'),
    url(r'^r3', webservice3, name='webservice3'),
]
